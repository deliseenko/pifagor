if [ ! $# == 2 ]; then
  echo "Usage: $0 <function name> <argument>"
  exit
fi

echo $2 > arg.pfg;

./trans2 -c 'arg.pfg' erra.log dbga.log;

for FN in `ls *.pfg`; do
	if [ $FN != "arg.pfg" ]; then
		./trans2 "-t" $FN;
	fi 
done

for rigFile in `find . -name '*.rig'`; do
	if [ $rigFile != "./arg.rig" ]; then
		cgFile="${rigFile%'.rig'}.cg"
		echo "cg data " $rigFile $cgFile
		./cgen2 "-f" $rigFile $cgFile
	  	./rig2dot -png $rigFile
		./cg2dot -png $cgFile
	fi
done

./inter2 $1 
